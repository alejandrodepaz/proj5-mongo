"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask, sys
from flask import Flask, request, render_template, redirect, url_for, flash
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import os
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():

    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('distance',type=str)
    brevet_start_date = request.args.get('begin_date', type=str)
    brevet_start_time = request.args.get('begin_time', type=str)
    brevet_start = brevet_start_date + " " + brevet_start_time

    if km > int(dist[:-2])*1.2:
        result = {"open": 0, "close": 0}
        return flask.jsonify(result = result)

    if km > int(dist[:-2]) and km <= int(dist[:-2])*1.2:
        km = int(dist[:-2])
    
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, int(dist[:-2]), brevet_start)
    close_time = acp_times.close_time(km, int(dist[:-2]), brevet_start)

    if km ==int(dist[:-2]):
        close = arrow.get(close_time)
        if km == 200:
            close = close.shift(minutes =+ 10)
            close_time = close.isoformat()
        elif km == 400:
            close = close.shift(minutes =+ 20)
            close_time = close.isoformat()

    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


#############
open_confirmed = []
close_confirmed = []

@app.route('/display', methods=["POST"])
def todo():
    global db, client, open_confirmed, close_confirmed
    
    length = len(open_confirmed)
    if length == 0:
        flash("Database is Empty, Please Submit Form with Values. Database Clears After Invocation of 'Display'.")
        return redirect(url_for("index"))
    
    _items = db.tododb.find()
    items = [item for item in _items]

    db.tododb.drop()
    db = client.tododb
    
    open_confirmed = []
    close_confirmed = []
    
    flash("Display Button Was Clicked")
    return flask.render_template('db_display.html', items=items)


@app.route('/new', methods = ["POST"])
def new():


    global open_confirmed, close_confirmed
    
    open_time = request.form.getlist("open")
    close_time = request.form.getlist("close")

    open_times = []
    close_times = []

    for i in open_time:
        i = str(i)
        if (i != "") and (i not in open_confirmed):
            open_times.append(i)

    for j in close_time:
        j = str(j)
        if (j != "") and (j not in close_confirmed):
            close_times.append(j)

    length = len(open_times)

    if len(open_times) == 0 or len(close_times) == 0:
        flash("No New Controls Were Given")
        return redirect(url_for("index"))
    
        
    for k in range(length):

        open_confirmed.append(open_times[k])
        close_confirmed.append(close_times[k])

        times = {"open_time": open_times[k], "close_time": close_times[k]}

        db.tododb.insert_one(times)

    flash("Submit Button Was Clicked!")    
            
    return redirect(url_for("index"))



app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
